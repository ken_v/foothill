package com.company;
import java.util.Scanner;

public class Main {

    // Static "Globals"
    public static int MAX_CAPACITY = 555;

    public static void main(String[] args) {

        Seating s = new Seating();
        s.get_head_count();
        s.calculate_seats();
        s.display_seats();
    }
}

class Seating
{

    private int passengers;
    private int employeePasses;
    private int availableSeats;

    // Get the number of occupied seats.
    public void get_head_count()
    {
        Scanner input = new Scanner(System.in);
        System.out.print("Enter the number of ticketed passengers: ");
        passengers = input.nextInt();
        System.out.print("Enter the number of comp. employee passes: ");
        employeePasses = input.nextInt();
        System.out.println("");

    }

    // Calculate the available seating.
    public void calculate_seats()
    {
        availableSeats = Main.MAX_CAPACITY - (passengers + employeePasses);
    }

    // Display the available seats.
    public void display_seats()
    {
        System.out.println("Available seats: " + availableSeats);
    }



}