/*
Assignment 2: PLAY GOLF?
AUTHOR : KENNETH VERBOSKY
VERSION : 1
DATE : 2013-10-23T21:54:03
CLASS : CS 001A 21065 LM F13
This program prompts the user for today's weather data
and calculates how many golfers to expect for the day. 
*/

package com.kenneth_verbosky;

import java.math.*;
import java.util.Scanner;



public class Main {


    public static void swapShells(int n1, int n2)
    {
        int temp = n1;
        n1 = n2;
        n2 = temp;
    }

    public static void swapShellsFirstInArray(int[] array)
    {
        int temp = array[0];
        array[0] = array[1];
        array[1] = temp;
    }
    public static void main(String[] args) {

        int[] a = {1,2};

        swapShells(a[0], a[1]);
//        swapShellsFirstInArray(a);
        System.out.println(a[0]);
        System.out.println(a[1]);
        Today t = new Today();
//        t.get_user_input();

    }

}

class Today
{
    private int active_members;
    private double expected_players;
    private int temperature = 100;
    private int wind_speed;
    private int humiditiy;
    private int weather_outlook;
    private String weather_outlook_pretty; // String for echoing the weather


    public void get_user_input()
    {
        int temperature = 1;
        System.out.println(temperature);
        Scanner input = new Scanner(System.in);
        System.out.println("How many active club members are there?");
        active_members = input.nextInt();

        System.out.println("What's the weather like? \nEnter 1 for Sunny\nEnter 2 for Overcast\nEnter 3 for rain:");
        weather_outlook = input.nextInt();

        System.out.println("What's the current air temperature in degrees F? ");
        temperature = input.nextInt();

        System.out.println("What's the current wind speed in mph? ");
        wind_speed = input.nextInt();

        System.out.println("What's the current relative humiditiy? ");
        humiditiy = input.nextInt();
    }
    public void calculate_players()
    {
        // Use coefficient vars to make calculations cleaner. 
        double sunny_coefficient = 0.25;
        double overcast_coefficient = 0.12;
        double rain_coefficient = 0.03;

        if (temperature < 32) {
            sunny_coefficient = 0;
            overcast_coefficient = 0;
            rain_coefficient = 0;
        }
        // Bonus: check for excessive wind or humidity.
        if (wind_speed >= 20) {
            sunny_coefficient -= 0.02;
            overcast_coefficient -= 0.02;
        }

        if (humiditiy > 52 && temperature >= 90) {
            sunny_coefficient -= 0.06;
            overcast_coefficient -= 0.06;
        }

        // Calculate the number of expected players. 
        switch (weather_outlook) {
            case 1: expected_players = active_members * sunny_coefficient;
                weather_outlook_pretty = "Sunny";
                break;
            case 2: expected_players = active_members * overcast_coefficient;
                weather_outlook_pretty = "Overcast";
                break;
            case 3: expected_players = active_members * rain_coefficient;
                weather_outlook_pretty = "Raining";
                break;
            default: break;
        }

    }

    public void display_output()
    {
        System.out.println("The weather is: " + weather_outlook_pretty);
        System.out.println("Expected Players: " + (int) Math.round(expected_players));
    }
}