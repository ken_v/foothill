
/*
Assignment 4: I/O with web server logfile
AUTHOR : KENNETH VERBOSKY
VERSION : 1
DATE : 2013-12-01T20:59:22
CLASS : CS 001A 21065 LM F13
DESCRIPTION: This programs opens a log file and outputs a
    sorted version of the log to a new file.

Logger.java is the file handler + text processing class.
*/

package com.verbosky.kenneth;

import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class Logger{

    int lineCount;               // log file line count
    String logFileString = "";   // temp string container for log file
    String[] logSplitLines;      // String array used for sorting
    Boolean arrayIsSorted;       // Flag for testing if array is sorted

    public void openFile(String source) throws Exception
    {
        File sourceFile = new File(source);

        if(!sourceFile.exists())
        {
            //display error
            System.out.println("source not found");
            System.exit(2);
        }


        Scanner input = new Scanner(sourceFile);
//        PrintWriter output = new PrintWriter(targetFile);

        // Count the lines with a while loop, so that a string array can be initialized.
        System.out.println("Counting lines.");
        while(input.hasNext())
        {
            lineCount++;
            logFileString += input.nextLine();
            logFileString += "\n";
        }

        System.out.println("Lines: " + lineCount);
        logSplitLines = logFileString.split("\n");
    }
    public void sortLog()
    {
        Arrays.sort(logSplitLines);
        arrayIsSorted = true;
    }

    public void exportSortedLog(String outputFile) throws Exception
    {

        if (!arrayIsSorted) {
            System.out.println("Array is not sorted. This will simply duplicate the input file.");
        }


        // Skip the exist check, since the following code will
        // simply create a new output file if one does not exist.

        // THIS CODE IS FROM DEMO MODULE
        PrintWriter pw = null;
        pw = new PrintWriter(new PrintWriter(outputFile));

        for (int i = 0; i < logSplitLines.length; i++) {

            pw.println(logSplitLines[i]);

        }
        pw.flush();
        pw.close();
        // ^^ DEMO CODE ^^
    }



}