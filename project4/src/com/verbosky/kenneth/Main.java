
/*
Assignment 4: I/O with web server logfile
AUTHOR : KENNETH VERBOSKY
VERSION : 1
DATE : 2013-12-01T20:59:22
CLASS : CS 001A 21065 LM F13
DESCRIPTION: This programs opens a log file and outputs a
    sorted version of the log to a new file.

Logger.java is the file handler + text processing class.
*/

package com.verbosky.kenneth;

public class Main {

    public static void main(String[] args) {

        Logger log = new Logger();

        try
        {
            log.openFile("weblog.txt");
            log.sortLog();
            log.exportSortedLog("weblog-sorted.txt");
        }
        catch(Exception ex)
        {
            System.out.println("File Error: ");
            System.out.println(ex);
        }


    }
}