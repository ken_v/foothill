/*
Assignment 3: Hide Phone Number
AUTHOR : KENNETH VERBOSKY
VERSION : 1
DATE : 2013-11-17T20:33:59
CLASS : CS 001A 21065 LM F13
This program prompts the user for a phone number and encryption key,
then encrypts and decrypts the phone number.
*/
package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	Crypto phone = new Crypto();
        phone.get_user_input();
        System.out.println("Phone Number: " + phone.number_pretty);
        phone.encrypt_phone(phone.phone_number, phone.crypto_key);
        System.out.println("Encrypted Number: " + phone.encrypted_pretty);
        System.out.println("Decrypting phone number with key: " + phone.key_pretty);
        phone.decrypt_phone(phone.phone_number, phone.crypto_key);
        System.out.println("Decrypted Number: " + phone.decrypted_pretty);
    }
}

class Crypto
{

    // Int arrays for crypto processing.
    public int[] phone_number;
    public int[] crypto_key;

    // Use strings for nicer output.
    public String number_pretty;
    public String key_pretty;
    public String encrypted_pretty;
    public String decrypted_pretty;

    public void get_user_input()
    {
        // We first ask the user for the entire phone number.
        // In the console, this allows the user to input the entire phone number,
        // as a single string, then press Enter. Ditto for the encryption key.
        // After we scan the string input, convert it to a int[] for processing.

        Scanner input = new Scanner(System.in);
        System.out.println("Enter the phone number to encrypt: (e.g. 7071234567)");
        number_pretty = input.next();

        System.out.println("Enter a 3 digit encryption key: (135)");
        key_pretty = input.next();
        convert_inputs();
    }

    public void convert_inputs()
    {
        // Convert the inputs to int[] instance vars. These vars are processed in
        // encrypt_phone() and decrypt_phone().
        phone_number =  new int[10];
        for (int n=0; n<10; n++) {
            phone_number[n] = Character.digit(number_pretty.charAt(n), 10);
        }

        crypto_key = new int[3];
        for (int k=0; k<3; k++) {
            crypto_key[k] = Character.digit(key_pretty.charAt(k), 10);
        }
    }

    public void encrypt_phone(int[] raw_phone, int[] key)
    {
        // Encryption algorithm:
        // C = (P + key) % 10

        for (int i = 0; i < 10; i++) {
            if (i < 3) { // Encrypt area code
                phone_number[i] = (raw_phone[i] + key[0]) % 10;
            }
            else if (i >= 3 && i < 6) { // Exchange
                phone_number[i] = (raw_phone[i] + key[1]) % 10;
            }
            else { // Subscriber number
                phone_number[i] = (raw_phone[i] + key[2]) % 10;
            }
        }

        // Convert the int[] to string for display
        encrypted_pretty = "";
        for (int c = 0; c < phone_number.length; c++) {
            encrypted_pretty += phone_number[c];
        }
    }

    public void decrypt_phone(int[] enc_phone, int[] key)
    {
        // Decryption algorithm:
        // P = (C - key) % 10
        // if(P < 0)     P= P + 10;

        for (int i = 0; i < 10; i++) {
            if (i < 3) { // Area code
                phone_number[i] = (enc_phone[i] - key[0]) % 10;
            }
            else if (i >= 3 && i < 6) { // Exchange
                phone_number[i] = (enc_phone[i] - key[1]) % 10;
            }
            else { // Subscriber number
                phone_number[i] = (enc_phone[i] - key[2]) % 10;
            }
            // Catch negative numbers.
            if (phone_number[i] < 0) {
                phone_number[i] += 10;
            }
        }

        // Convert the int[] to string for display
        decrypted_pretty = "";
        for (int c = 0; c < phone_number.length; c++) {
            decrypted_pretty += phone_number[c];
        }
    }
}
